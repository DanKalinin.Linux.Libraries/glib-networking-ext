//
// Created by root on 19.09.2021.
//

#include "gneinit.h"

void gne_init(void) {
    gne_g_init();
    g_io_gnutls_load(NULL);
    g_io_gnomeproxy_load(NULL);
}

void gne_init_i18n(gchar *directory) {
    gne_g_init_i18n(directory);
}

void gne_init_po(void) {
    gne_g_init_po();
}

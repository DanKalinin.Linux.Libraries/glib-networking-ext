//
// Created by Dan on 10.03.2025.
//

#include "gneginit.h"
#include "gnegi18n.h"

void gne_g_init(void) {

}

void gne_g_init_i18n(gchar *directory) {
    (void)bindtextdomain(GETTEXT_PACKAGE, directory);
    (void)bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
}

void gne_g_init_po(void) {
    gchar *directory = bindtextdomain(GETTEXT_PACKAGE, NULL);
    gchar *language = (gchar *)g_getenv("LANG");
    g_autofree gchar *file = g_build_filename(directory, language, "LC_MESSAGES", GETTEXT_PACKAGE ".mo", NULL);
    if (!gne_g_lc_messages_set(file, NULL)) return;
}

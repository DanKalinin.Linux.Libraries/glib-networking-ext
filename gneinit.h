//
// Created by root on 19.09.2021.
//

#ifndef LIBRARY_GLIB_NETWORKING_EXT_GNEINIT_H
#define LIBRARY_GLIB_NETWORKING_EXT_GNEINIT_H

#include "gnemain.h"
#include "gnegnutls.h"
#include "gnegnomeproxy.h"
#include "gneginit.h"

G_BEGIN_DECLS

void gne_init(void);
void gne_init_i18n(gchar *directory);
void gne_init_po(void);

G_END_DECLS

#endif //LIBRARY_GLIB_NETWORKING_EXT_GNEINIT_H

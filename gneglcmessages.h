//
// Created by Dan on 10.03.2025.
//

#ifndef LIBRARY_GLIB_NETWORKING_EXT_GNEGLCMESSAGES_H
#define LIBRARY_GLIB_NETWORKING_EXT_GNEGLCMESSAGES_H

#include "gnemain.h"

G_BEGIN_DECLS

extern gchar gne_g_lc_messages_ru_data[];
extern gint gne_g_lc_messages_ru_n;

gboolean gne_g_lc_messages_set(gchar *file, GError **error);

G_END_DECLS

#endif //LIBRARY_GLIB_NETWORKING_EXT_GNEGLCMESSAGES_H

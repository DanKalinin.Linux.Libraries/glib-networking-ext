//
// Created by dan on 15.12.2019.
//

#ifndef LIBRARY_GLIB_NETWORKING_EXT_GLIB_NETWORKING_EXT_H
#define LIBRARY_GLIB_NETWORKING_EXT_GLIB_NETWORKING_EXT_H

#include <glib-networking-ext/gnemain.h>
#include <glib-networking-ext/gnegnutls.h>
#include <glib-networking-ext/gnegnomeproxy.h>
#include <glib-networking-ext/gneinit.h>

#endif //LIBRARY_GLIB_NETWORKING_EXT_GLIB_NETWORKING_EXT_H

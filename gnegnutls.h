//
// Created by dan on 15.12.2019.
//

#ifndef LIBRARY_GLIB_NETWORKING_EXT_GNEGNUTLS_H
#define LIBRARY_GLIB_NETWORKING_EXT_GNEGNUTLS_H

#include "gnemain.h"

G_BEGIN_DECLS

extern void g_io_gnutls_load(GIOModule *self);

G_END_DECLS

#endif //LIBRARY_GLIB_NETWORKING_EXT_GNEGNUTLS_H

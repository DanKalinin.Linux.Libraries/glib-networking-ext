//
// Created by Dan on 10.03.2025.
//

#ifndef LIBRARY_GLIB_NETWORKING_EXT_GNEGI18N_H
#define LIBRARY_GLIB_NETWORKING_EXT_GNEGI18N_H

#include "gnemain.h"

G_BEGIN_DECLS

#define GETTEXT_PACKAGE "glib-networking"
#include <glib/gi18n-lib.h>

G_END_DECLS

#endif //LIBRARY_GLIB_NETWORKING_EXT_GNEGI18N_H

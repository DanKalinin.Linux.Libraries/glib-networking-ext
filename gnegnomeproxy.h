//
// Created by Dan on 05.06.2022.
//

#ifndef LIBRARY_GLIB_NETWORKING_EXT_GNEGNOMEPROXY_H
#define LIBRARY_GLIB_NETWORKING_EXT_GNEGNOMEPROXY_H

#include "gnemain.h"

G_BEGIN_DECLS

extern void g_io_gnomeproxy_load(GIOModule *self);

G_END_DECLS

#endif //LIBRARY_GLIB_NETWORKING_EXT_GNEGNOMEPROXY_H

//
// Created by Dan on 10.03.2025.
//

#ifndef LIBRARY_GLIB_NETWORKING_EXT_GNEGINIT_H
#define LIBRARY_GLIB_NETWORKING_EXT_GNEGINIT_H

#include "gnemain.h"
#include "gneglcmessages.h"

G_BEGIN_DECLS

void gne_g_init(void);
void gne_g_init_i18n(gchar *directory);
void gne_g_init_po(void);

G_END_DECLS

#endif //LIBRARY_GLIB_NETWORKING_EXT_GNEGINIT_H
